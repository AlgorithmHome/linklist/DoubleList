public class ListNode {
    int val;
    ListNode next = null;
    ListNode last = null;

    ListNode(int val) {
        this.val = val;
    }
}