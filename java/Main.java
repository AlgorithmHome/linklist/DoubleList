
public class Main {

    public static void main(String[] args) {
        ListNode head = new ListNode(2);
        ListNode temp = head;
        for (int i=0;i<10;i++){
            temp.next = new ListNode(4+i);
            temp.next.last  =temp;
            temp = temp.next;
        }
        //正向输出
        ListNode tempHead = head;
        while (tempHead!=null){
            System.out.println(tempHead.val);
            tempHead = tempHead.next;
        }
        System.out.println("-------------------");
        //反向输出
        ListNode tempIndex = temp;
        while (tempIndex!=null){
            System.out.println(tempIndex.val);
            tempIndex = tempIndex.last;
        }
    }
}